'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Show Schema
 */
var ShowSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    name: {
        type: String,
        default: '',
        trim: true
    },
    description: {
        type: String,
        default: '',
        trim: true
    },
    theater: {
        type: Schema.ObjectId,
        ref: 'Theater'
    }
});

/**
 * Validations
 */
ShowSchema.path('name').validate(function(name) {
    return name.length;
}, 'Name cannot be blank');

/**
 * Statics
 */
ShowSchema.statics.load = function(id, cb) {
    this.findOne({
        _id: id
    }).populate('theater', 'name username').exec(cb);
};

mongoose.model('Show', ShowSchema);