'use strict';

// Theaters routes use theaters controller
var theaters = require('../controllers/theaters');
var authorization = require('./middlewares/authorization');

module.exports = function(app) {

    app.get('/theaters', theaters.all);
    app.post('/theaters', theaters.create);
    app.get('/theaters/:theaterId', theaters.show);
    app.put('/theaters/:theaterId', theaters.update);
    app.del('/theaters/:theaterId', theaters.destroy)

    app.param('theaterId', theaters.theater);

};