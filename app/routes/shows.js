'use strict';

var shows = require('../controllers/shows');
var authorization = require('./middlewares/authorization');

module.exports = function(app) {

    app.get('/shows', shows.all);
    app.post('/shows', shows.create);
    app.get('/shows/:showId', shows.show);
    app.put('/shows/:showId', shows.update);
    app.del('/shows/:showId', shows.destroy);

    app.param('showId', shows.findShow);

};