'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Theater = mongoose.model('Theater'),
    _ = require('lodash');


/**
 * Find theater by id
 */
exports.theater = function(req, res, next, id) {
    Theater.load(id, function(err, theater) {
        if (err) return next(err);
        if (!theater) return next(new Error('Failed to load theater ' + id));
        req.theater = theater;
        next();
    });
};

/**
 * Create a theater
 */
exports.create = function(req, res) {
    var theater = new Theater(req.body);

    theater.save(function(err) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(theater);
        }
    });
};

/**
 * Update a theater
 */
exports.update = function(req, res) {
    var theater = req.theater;

    theater = _.extend(theater, req.body);

    theater.save(function(err) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(theater);
        }
    });
};

/**
 * Delete a theater
 */
exports.destroy = function(req, res) {
    var theater = req.theater;

    theater.remove(function(err) {
        if (err) {
            return res.send('users/signup', {
                errors: err.errors,
                theater: theater
            });
        } else {
            res.jsonp(theater);
        }
    });
};

/**
 * Show a theater
 */
exports.show = function(req, res) {
    res.jsonp(req.theater);
};

/**
 * List of Theaters
 */
exports.all = function(req, res) {
    Theater.find().sort('-created').exec(function(err, theaters) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(theaters);
        }
    });
};