'use strict';

//Theaters service used for articles REST endpoint
angular.module('mean.theaters').factory('Theaters', ['$resource', function($resource) {
    return $resource('theaters/:theaterId', {
        theaterId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);