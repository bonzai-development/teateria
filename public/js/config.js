'use strict';

//Setting up route
angular.module('mean').config(['$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    // For unmatched routes:
    $urlRouterProvider.otherwise('/');

    // states for my app
    $stateProvider
      .state('theater by id', {
        url: '/theaters/:theaterId',
        templateUrl: 'views/theaters/view.html'
    })
      .state('all shows', {
        url: '/shows',
        templateUrl: 'views/shows/list.html'
    })
      .state('show by id', {
        url: '/shows/:showId',
        templateUrl: 'views/shows/view.html'
    })
      .state('user settings', {
        url: '/users/settings',
        templateUrl: 'views/users/settings.html'
    })
      .state('about', {
        url: '/about',
        templateUrl: 'views/static/about.html'
    })
      .state('terms', {
        url: '/terms',
        templateUrl: 'views/static/terms.html'
    })
      .state('contact', {
        url: '/contact',
        templateUrl: 'views/static/contact.html'
    })
      .state('home', {
        url: '/',
        templateUrl: 'views/shows/list.html'
    });

}
]);

//Setting HTML5 Location Mode
angular.module('mean').config(['$locationProvider',
  function($locationProvider) {
    $locationProvider.hashPrefix('!');
}
]);
