'use strict';

angular.module('mean.theaters').controller('TheatersController', ['$scope', '$stateParams', '$location', 'Global', 'Theaters', function ($scope, $stateParams, $location, Global, Theaters) {
    $scope.global = Global;

    $scope.findOne = function() {
        Theaters.get({
            theaterId: $stateParams.theaterId
        }, function(theater) {
            $scope.theater = theater;
        });
    };
}]);