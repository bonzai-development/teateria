'use strict';

angular.module('mean', ['ngCookies', 'ngResource', 'ui.bootstrap', 'ui.router', 'mean.system', 'mean.theaters', 'mean.users', 'mean.shows']);

angular.module('mean.system', []);
angular.module('mean.theaters', []);
angular.module('mean.users', []);
angular.module('mean.shows', []);

// FIXME: The controllers/theater.js and services/theaters.js files are not
// being loaded in, thus throwing an undefined exception.

angular.module('mean.theaters').factory('Theaters', ['$resource', function($resource) {
    return $resource('theaters/:theaterId', {
        theaterId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);

angular.module('mean.theaters').controller('TheatersController', ['$scope', '$stateParams', '$location', 'Global', 'Theaters', function ($scope, $stateParams, $location, Global, Theaters) {
    $scope.global = Global;

    $scope.findOne = function() {
        Theaters.get({
            theaterId: $stateParams.theaterId
        }, function(theater) {
            $scope.theater = theater;
        });
    };
}]);

// TODO: Move the below into seperate files.

angular.module('mean.users').factory('Users', ['$resource', function($resource) {
    return $resource('users/me', {}, {
        update: {
            method: 'PUT'
        }
    });
}]);

angular.module('mean.users').controller('UsersController', ['$scope', '$http', '$stateParams', '$location', 'Global', 'Users', function ($scope, $http, $stateParams, $location, Global, Users) {
    $scope.global = Global;

    $scope.update = function() {
        var user = $scope.user;
        if (!user.updated) {
            user.updated = [];
        }
        user.updated.push(new Date().getTime());

        user.$update(function() {
            $location.path('users/settings');
        });
    };

    $scope.findOne = function() {
        Users.get({}, function(user) {
            $scope.user = user;
        });
    };
}]);

angular.module('mean.shows').factory('Shows', ['$resource', function($resource) {
    return $resource('shows/:showId', {
        showId: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
}]);

angular.module('mean.shows').controller('ShowsController', ['$scope', '$stateParams', '$location', 'Global', 'Shows', function ($scope, $stateParams, $location, Global, Shows) {
    $scope.global = Global;

    $scope.find = function() {
        Shows.query(function(shows) {
            $scope.shows = shows;
        });
    };

    $scope.findOne = function() {
        Shows.get({
            showId: $stateParams.showId
        }, function(show) {
            $scope.show = show;
        });
    };
}]);